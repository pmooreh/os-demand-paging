default : compile0;

test : test0;

compile% :
	rm -f kernel/kernel*
	ln k$*.cc kernel/kernel.cc
	(make -C kernel kernel.img)
	(make -C fat439 clean all)

test% : compile%
	-@echo -n "$@ ... "
	@(make -C fat439 clean all)
	@expect e0.tcl > k$*.raw
	-@egrep '^\*\*\*' k$*.raw > k$*.out 2>&1
	-@((diff -b k$*.out k$*.ok > k$*.diff 2>&1) && echo "pass") || echo "failed, look at k$*.raw, k$*.ok, k$*.out, and k$*.diff for more information"
	
% :
	(make -C kernel $@)
	(make -C fat439 $@)

clean:
	-(make -C kernel clean)
