#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "vmm.h"
#include "ide.h"
#include "fat439.h"

void kernelMain(void) {

    Debug::say("test0");
    StrongPtr<BlockIO> d = IDE::d();
    StrongPtr<Fat439> fs { new Fat439(d) };
    StrongPtr<Directory> root = fs->rootdir;

    StrongPtr<AddressSpace> vm { new AddressSpace() };

    AddressSpace::activate(vm);

    {
        StrongPtr<File> f = root->lookupFile("hello.txt");
        vm->map(0x90000000, f);

        char *ptr = (char*) 0x90000000;

        char x = *ptr;

        if (*ptr != 'h') {
            Debug::say("mem[%p] = %c",ptr,x);
        }
        Debug::say("%s",ptr);
    }

    {
        StrongPtr<File> f = root->lookupFile("big");
        vm->map(0x80000000, f);

        char* ptr = (char*) 0x80000000;

        for (size_t off = 24; off < f->getLength(); off += 8 * 5000) {
             char buf[8];
             for (int i=0; i<7; i++) {
                 buf[i] = ptr[off+i];
             }
             buf[7] = 0;
             Debug::say("%s",buf);
        }
    }


}

/* Called when the system is about to shutdown */
void kernelTerminate() {
}
