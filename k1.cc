#include "stdint.h"
#include "debug.h"
#include "refs.h"
#include "vmm.h"
#include "ide.h"
#include "fat439.h"
#include "machine.h"

volatile int* count;

void boop(size_t len) {
    char buf[71];
    int j = 0;
    char* ptr = (char*) 0x80000000;
    
    for (size_t off = 24; off < len; off += 8 * 10000) {
         for (int i=0; i<7; i++) {
             buf[j] = ptr[off+i];
             ++j;
         }
    }
    buf[70] = 0;
    Debug::say("result: %s", buf);
    getThenIncrement(count, 1);
}

void kernelMain(void) {

    Debug::say("test1");

    StrongPtr<BlockIO> d = IDE::d();
    StrongPtr<Fat439> fs { new Fat439(d) };
    StrongPtr<Directory> root = fs->rootdir;

    StrongPtr<AddressSpace> vm { new AddressSpace() };

    AddressSpace::activate(vm);

    count = new int;
    *count = 0;

    StrongPtr<File> f = root->lookupFile("big");
    vm->map(0x80000000, f);
    for(size_t i = 0; i < 30; ++i) {
        threadCreate<size_t>(boop, f->getLength());
    }

    while(*count < 30) {
      Thread::yield();
    }
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
}