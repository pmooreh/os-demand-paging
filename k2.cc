#include "stdint.h"
#include "debug.h"
#include "refs.h"
#include "vmm.h"
#include "ide.h"
#include "fat439.h"
#include "machine.h"

volatile int* count;

struct MyStruct {
    StrongPtr<File> f;
    size_t memIndex;
    StrongPtr<AddressSpace> vm;
};

void boop2(uint32_t start) {
    char buf[71];
    int j = 0;
    char* ptr = (char*)start;
    
    for (size_t off = 24; off < 800008; off += 8 * 10000) {
         for (int i=0; i<7; i++) {
             buf[j] = ptr[off+i];
             ++j;
         }
    }
    buf[70] = 0;
    Debug::say("result: %s", buf);
    getThenIncrement(count, 1);
}

void boop(MyStruct s) {
    StrongPtr<File> f = s.f;
    size_t i = s.memIndex;
    StrongPtr<AddressSpace> vm = s.vm;
    vm->map(0x80000000 + 0xC8000 * i, f);

    for(size_t j = 0; j < 3; ++j) {
        threadCreate<uint32_t>(boop2, 0x80000000 + 0xC8000 * i);
    }
}

void kernelMain(void) {

    Debug::say("test2");

    StrongPtr<BlockIO> d = IDE::d();
    StrongPtr<Fat439> fs { new Fat439(d) };
    StrongPtr<Directory> root = fs->rootdir;

    StrongPtr<AddressSpace> vm { new AddressSpace() };

    AddressSpace::activate(vm);

    count = new int;
    *count = 0;

    
    StrongPtr<File> f = root->lookupFile("big");
    for(size_t i = 0; i < 10; ++i) {
        MyStruct s;
        s.f = f;
        s.memIndex = i;
        s.vm = vm;
        threadCreate<MyStruct>(boop, s);
    }

    while(*count < 30) {
      Thread::yield();
    }
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
}

