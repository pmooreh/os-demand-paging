#include "alarm.h"
#include "pit.h"
#include "locks.h"
#include "thread.h"

BlockingLock *Alarm::lock = nullptr;
Alarm *Alarm::first = nullptr;

void alarmThread(void) {
    while (Thread::alive)  {
        Alarm::check();
        Thread::yield();
    }
}

void Alarm::init() {
    lock = new BlockingLock();
    first = nullptr;
    threadCreate(alarmThread);
}

void Alarm::check(void) {
        Alarm::lock->lock();
        while ((first != nullptr) && (Pit::ticks >= first->at)) {
            Alarm* p = first;
            first = first->next;
            p->event->signal();
            delete p;
        }
        Alarm::lock->unlock();
}


Alarm::Alarm(uint32_t at, StrongPtr<Event> event) :
    at(at), event(event)
{
}

void Alarm::schedule(uint32_t after, StrongPtr<Event> event) {
    uint32_t at = Pit::secondsToTicks(after) + Pit::ticks;
    auto a = new Alarm(at, event);
    // O(n) until we learn about red-black trees 

    lock->lock();

    Alarm *p = first;
    Alarm **pPrev = &first;

    while (p != nullptr) {
        if (at < p->at) break;
        pPrev = &p;
        p = p->next;
    }
    a->next = p;
    *pPrev = a;

    lock->unlock();
}
