#include "fat439.h"
#include "debug.h"
#include "heap.h"

#define BLOCK_SIZE 4096

bool checkStringEquivalence(const char* s1, const char* s2, int len) {
	bool same = true;
	for (int i = 0; i < len; i++) {
		if (s1[i] == 0 && s2[i] == 0)
			break;
		if (s1[i] == 0 || s2[i] == 0 || s1[i] != s2[i]) {
			same = false;
			break;
		}
	}
	return same;
}

/*************************/
/* Fat439 implementation */
/*************************/

Fat439::Fat439(StrongPtr<BlockIO> dev) : FileSystem(dev){
	/*	to initialize FAT file system, read in block 0 and set as the superblock
    	then allocate enough for the file allocation table
    */

    // read in block 0
    StrongPtr<BlockBuffer> buffer = dev->readBlock(0);
    // wait on block buffer's ready event
    buffer->ready->wait();
    // copy data over
    superBlock = StrongPtr<SuperBlock>((SuperBlock*)buffer->data);


    // Debug::printf("%c%c%c%c\n",superBlock->magic[0],superBlock->magic[1],superBlock->magic[2],superBlock->magic[3]);

    // Debug::printf("nBlocks: %d\n", superBlock->nBlocks);

    // Debug::printf("avail: %d\n", superBlock->avail);

    // Debug::printf("rootDir: %d\n", superBlock->rootDir);

    // size of fat is 1 int for every block
    size_t fatSize = 4 * superBlock->nBlocks;
    fat = StrongPtr<Fat>(new Fat);
    fat->blockList = (size_t*)malloc(fatSize);

    // calculate number of blocks needed for the fat
    // number of blocks * 4 bytes per block / 4096 bytes per block
    int numFatBlocks = superBlock->nBlocks * 4 / BLOCK_SIZE;

    // read in all the FAT blocks
    dev->readAll(BLOCK_SIZE, fat->blockList, numFatBlocks * BLOCK_SIZE);

    // for (int i = 0; i < (int)superBlock->nBlocks; i++) {
    // 	Debug::printf("    FAT[%d]: %d\n", i, fat->blockList[i]);
    // }

    rootdir = StrongPtr<Directory>(new FatDirectory(superBlock->rootDir, fat, dev));
}

/************
FAT DIRECTORY
************/

FatDirectory::FatDirectory(size_t blockNumber, StrongPtr<Fat> fat, StrongPtr<BlockIO> dev)
		: Directory(), FatFile::FatFile(blockNumber, fat, dev) {}


size_t FatDirectory::nEntries() {
	// read in the first block of this directory
	StrongPtr<BlockBuffer> buffer = dev->readBlock(startBlock);
	// wait on the input
	buffer->ready->wait();

	// now pick the size off the front
	// 8 byte header, bytes 4 - 7 are the size
	size_t directorySizeBytes = ((uint32_t*)buffer->data)[1];

	// each entry in the directory is 16 bytes, ??not sure if header is included??
	return directorySizeBytes / 16;
}

// NEEDS WORK: DOESN'T ACCOUNT FOR A TOO SHORT NAME BUFFER //
void FatDirectory::entryName(size_t i, char* name) {
	// get the block that the entry will live in
	// this is (i * size of dir entry + the header) / block size
	int blockFileIndex = (i * 16 + 8) / BLOCK_SIZE;

	// now traverse the FAT to get the block number
	size_t containingBlockNumber = FatFile::traverseFat(startBlock, blockFileIndex);

	// // read in that block
	// StrongPtr<BlockBuffer> buffer = fileSystem->dev->readBlock(containingBlockNumber);
	// // wait on the input
	// buffer->ready->wait();

	// // now grab the string from the buffer
	// int stringStartIndex = (i * 16 + 8) % BLOCK_SIZE;
	// for (int i = 0; i < 12; i++) {
	// 	name[i] = ((char*)buffer->data)[i + stringStartIndex];
	// }

	dev->readAll((i * 16 + 8 + BLOCK_SIZE * containingBlockNumber), name, 12);
	name[12] = 0;
}

size_t FatDirectory::lookupHelper(const char* name) {
	// just search this one directory
	StrongPtr<BlockBuffer> buffer = dev->readBlock(startBlock);
	buffer->ready->wait();

	// get the size then convert to numEntries
	size_t numEntries = ((size_t*)buffer->data)[1] / 16;

	// will write intermediate results here
	char currEntry[16];

	// loop through every entry
	for (size_t i = 0; i < numEntries; i++) {
		// offset into the file
		size_t offset = i * 16;
		// now just read 16 bytes!
		FatFile::readAll(offset, currEntry, 16);

		if (checkStringEquivalence(name, currEntry, 12))
			break;
	}

	// // loop through each entry in directory
	// for (size_t i = 0; i < numEntries; i++) {
	// 	size_t offset = (BLOCK_SIZE * startBlock) + (i * 16) + 8;
	// 	dev->readAll(offset, (void*)currEntry, 16);

	// 	if (checkStringEquivalence(name, currEntry, 12))
	// 		break;

	// 	// now inspect the entry!
	// 	// Debug::printf("   directory entry: ");
	// 	// for (int i = 0; i < 16; i++) {
	// 	// 	Debug::printf("%c", currEntry[i]);
	// 	// }
	// 	// Debug::printf("\n");

	// 	// if (checkStringEquivalence(name, currEntry, 12))
	// 	// 	Debug::printf("        YEP!!!\n");
	// 	// else
	// 	// 	Debug::printf("        NOPE!!\n");
	// }

	size_t ret = ((size_t*)currEntry)[3];
	// Debug::printf("    where does it live?: %d\n", ret);

	return ret;

	////////////////

	// size_t numBytes = ((size_t*)buffer->data)[1] + 8;
	// size_t numBlocks = numBytes / BLOCK_SIZE;
	// if  (numBytes % BLOCK_SIZE != 0)
	// 	numBlocks++;

	// // loop through each block
	// StrongPtr<BlockBuffer> currBlock = buffer;
	
}

StrongPtr<File> FatDirectory::lookupFile(const char* name) {
	size_t blockNum = FatDirectory::lookupHelper(name);
	return StrongPtr<File>(new FatFile(blockNum, fat, dev));
}

StrongPtr<Directory> FatDirectory::lookupDirectory(const char *name) {
	size_t blockNum = FatDirectory::lookupHelper(name);
	return StrongPtr<Directory>(new FatDirectory(blockNum, fat, dev));
}

/************
  FAT File
************/

FatFile::FatFile(size_t blockNumber, StrongPtr<Fat> fat, StrongPtr<BlockIO> dev)
		: File(), startBlock(blockNumber), fat(fat), dev(dev) {
	StrongPtr<BlockBuffer> buffer = dev->readBlock(startBlock);
	buffer->ready->wait();

	type = ((uint32_t*)buffer->data)[0];
	size = ((uint32_t*)buffer->data)[1];
} 

uint32_t FatFile::getType() {
	return type;
}
uint32_t FatFile::getLength() {
	return size;
}

size_t FatFile::read(size_t offset, void* buf, size_t nbyte) {

// Debug::printf("IN READ\n");

	size_t blockFileIndex = (offset + 8) / BLOCK_SIZE;
	size_t containingBlockNumber = FatFile::traverseFat(startBlock, blockFileIndex);
	size_t containgBlockOffset = (offset + 8) % BLOCK_SIZE;
	size_t totalOffset = containgBlockOffset + BLOCK_SIZE * containingBlockNumber;

	return dev->read(totalOffset, buf, nbyte);
}

size_t FatFile::readAll(size_t offset, void* buf, size_t nbyte) {

	// Debug::printf("In readAll\n");

	size_t overflow = 0;
	if (size < (offset + nbyte))
		overflow = offset + nbyte - size;

	// Debug::printf("    nbyte: %d\n", nbyte);
	// Debug::printf("    overflow: %d\n", overflow);

	// keep track of how many bytes still need to be read
	size_t remaining = nbyte - overflow;

	// Debug::printf("    remaining: %d\n", remaining);

	// now loop keep reading until all have been read
	while (remaining > 0) {

		// Debug::printf("    In readAll's read loop\n");

		// read into buf starting at nbyte - remaining
		void* bufStart = (void*)((size_t)buf + nbyte - remaining - overflow);
		// same with offset
		size_t off = offset + nbyte - remaining - overflow;

		remaining = remaining - FatFile::read(off, bufStart, remaining);

		// Debug::printf("        buf: %d\n", buf);
		// Debug::printf("        bufStart: %d\n", bufStart);
		// Debug::printf("        remaining: %d\n", remaining);
		// Debug::printf("\n");		
	}
	return nbyte - overflow;
}

// traverses the FAT, getting
size_t FatFile::traverseFat(size_t startBlock, size_t blockFileIndex) {
	size_t curr = startBlock;

	for(; blockFileIndex > 0; blockFileIndex--) {
		curr = fat->blockList[curr];
	}
	return curr;
}

size_t FatFile::nextInFat(size_t startBlock) {
	return traverseFat(startBlock, 1);
}
