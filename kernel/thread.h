#ifndef _THREAD_H_
#define _THREAD_H_

#include "stdint.h"
#include "refs.h"

class SpinLock;
class Event;

struct Thread {
    StrongPtr<Thread> next;
    //StrongPtr<Thread> me;
    uint32_t esp;
    uint32_t stack[1024];

    static volatile int nextId;

    void push(uint32_t v) {
        esp -= 4;
        *((uint32_t*) esp) = v;
    }

public:
    Thread();
    virtual ~Thread();

    virtual void run() = 0;

    StrongPtr<Event> exitEvent;
    StrongPtr<Event> deleteEvent;
    int id;

    static void exit(void);
    static void yield(void);
    static StrongPtr<Thread> start(Thread *);
    static void init(void);
    static void shutdown(void);
    static StrongPtr<Thread> current(void);

    static volatile bool alive;
    static int nCreate;
    static int nStart;
    static int nDelete;
};

template <class T>
class FuncThread : public Thread {
    void (*func)(T);
    T arg;
public:
    FuncThread(void (*func)(T), T arg) : func(func), arg(arg) {}
    void run() {
        func(arg);
    }
};

template <class T>
StrongPtr<Thread> threadCreate(void (*func)(T), T arg) {
    return Thread::start(new FuncThread<T>(func,arg));
}

class SimpleThread : public Thread {
    void (*func)(void);
public:
    SimpleThread(void (*func)(void)) : func(func) {}
    void run() {
        func();
    }
};

inline StrongPtr<Thread> threadCreate(void (*func)(void)) {
    return Thread::start(new SimpleThread(func));
}

extern void threadBlock(SpinLock *lock, bool was);
extern void threadReady(StrongPtr<Thread> t);
extern int threadId(void);

#endif
