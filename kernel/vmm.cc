#include "vmm.h"
#include "machine.h"
#include "idt.h"
#include "libk.h"
#include "locks.h"


/**************
FileRangeNode
*************/
FileRangeNode::FileRangeNode(StrongPtr<File> f, uint32_t vaStart) {
    file = f;
    start = vaStart;
    end = vaStart + f->getLength();
    next = StrongPtr<FileRangeNode>();
}
FileRangeNode::~FileRangeNode() {}

void FileRangeNode::printNode() {
    Debug::printf("        length: %d\n", file->getLength());
    Debug::printf("        va start: %x\n", start);
    Debug::printf("        va end: %x\n", end);
    Debug::printf("        next null? %s\n", next.isNull() ? "yes" : "no");
}

/**************
   FileList
*************/

FileList::FileList() : head(StrongPtr<FileRangeNode>()), last(StrongPtr<FileRangeNode>()),
    lock(StrongPtr<BlockingLock>(new BlockingLock())) {}
FileList::~FileList() {}

StrongPtr<FileRangeNode> FileList::getFileRangeNode(uint32_t va) {
    // assumption: list is non-empty
    lock->lock();
    StrongPtr<FileRangeNode> curr = head;

    // loop through each node in LL, find one whose range includes va
    while (!curr.isNull()) {
        if (va >= curr->start && va <= curr->end)
            break;
        curr = curr->next;
    }

    lock->unlock();
    return curr;
}

void FileList::addFileRangeNode(StrongPtr<FileRangeNode> node) {
    lock->lock();
    if (head.isNull()) {
        head = node;
        last = node;
        lock->unlock();
        return;
    }

    // add to end
    last->next = node;
    last = node;
    lock->unlock();
    return;
}

void FileList::printList() {
    lock->lock();
    StrongPtr<FileRangeNode> curr = head;
    int count = 0;

    while (!curr.isNull()) {
        Debug::printf("    fileNode index: %d\n", count);
        curr->printNode();
        Debug::printf("\n");

        curr = curr->next;
        count++;
    }

    lock->unlock();
}

/********
PhysMem
*******/

struct PhysMemNode {
    PhysMemNode* next;
};

struct PhysMemInfo {
    PhysMemNode *firstFree;
    uint32_t avail;
    uint32_t limit;
    BlockingLock lock;
    StrongPtr<AddressSpace> active;
};

static PhysMemInfo *info = nullptr;

void PhysMem::init(uint32_t start, uint32_t end) {
    info = new PhysMemInfo;
    info->avail = start;
    info->limit = end;
    info->firstFree = nullptr;

    /* register the page fault handler */
    IDT::trap(14,(uint32_t)pageFaultHandler);
}

uint32_t PhysMem::alloc() {
    uint32_t p;

    info->lock.lock();

    if (info->firstFree != nullptr) {
        p = (uint32_t) info->firstFree;
        info->firstFree = info->firstFree->next;
    } else {
        if (info->avail == info->limit) {
            Debug::panic("no more frames");
        }
        p = info->avail;
        info->avail += FRAME_SIZE;
    }
    info->lock.unlock();

    bzero((void*)p,FRAME_SIZE);

    return p;
}

void PhysMem::free(uint32_t p) {
    info->lock.lock();

    PhysMemNode* n = (PhysMemNode*) p;    
    n->next = info->firstFree;
    info->firstFree = n;

    info->lock.unlock();
}

/****************/
/* AddressSpace */
/****************/

void AddressSpace::activate(StrongPtr<AddressSpace> as) {
    info->active = as;
    vmm_on((uint32_t)as->pd);
}

AddressSpace::AddressSpace() : fileList(StrongPtr<FileList>(new FileList())), lock(StrongPtr<BlockingLock>(new BlockingLock())) {
    pd = (uint32_t*) PhysMem::alloc();
    for (uint32_t va = PhysMem::FRAME_SIZE;
        va < info->limit;
        va += PhysMem::FRAME_SIZE
    ) {
        pmap(va,va,false,true);
    }
}

AddressSpace::~AddressSpace() {
}

/* precondition: table is locked */
uint32_t& AddressSpace::getPTE(uint32_t va) {
    uint32_t i0 = (va >> 22) & 0x3ff;
    if ((pd[i0] & P) == 0) {
        pd[i0] = PhysMem::alloc() | 7; /* UWP */
    }
    uint32_t* pt = (uint32_t*) (pd[i0] & 0xfffff000);
    return pt[(va >> 12) & 0x3ff];
}

void AddressSpace::pmap(uint32_t va, uint32_t pa, bool forUser, bool forWrite) {
    invlpg(va);
    getPTE(va) = (pa & 0xfffff000) |
          (forUser ? U : 0) | (forWrite ? W : 0) | 1;
}

void AddressSpace::map(uint32_t va, StrongPtr<File> file) {
    // MISSING();
    //Debug::printf("call made to map, at va: %u\n", va);
    // create a range node, then add to fileList
    StrongPtr<FileRangeNode> node = StrongPtr<FileRangeNode>(new FileRangeNode(file, va));
    fileList->addFileRangeNode(node);

    //fileList->printList();
}

void AddressSpace::handlePageFault(uint32_t va) {
    AddressSpace::lock->lock();
    // check if the mapping has been established
    uint32_t pte = getPTE(va);
    // FIX THIS!!!!
    if (pte & 1) {
        lock->unlock();
        return;
    }


    Debug::say("page fault at %x",va);
    //Debug::printf("\n");
    //Debug::printf("call made to handlePageFault, at va: %u\n", va);
    StrongPtr<FileRangeNode> fileNode = fileList->getFileRangeNode(va);
    //Debug::printf("    found fileNode\n");
    //fileNode->printNode();
    
    // get physical frame
    uint32_t frame = PhysMem::alloc();

    // load data into this frame, size is 4096 bytes
    size_t diff = va - fileNode->start;
    size_t offset = diff - (diff % 4096);
    fileNode->file->readAll(offset, (void*)frame, 4096);

    // now pmap that ho
    AddressSpace::pmap(va, frame, false, true);
    AddressSpace::lock->unlock();
}

extern "C" void vmm_pageFault(uintptr_t va) {
    
    StrongPtr<AddressSpace> as = info->active;
    info->active->handlePageFault(va);
}
