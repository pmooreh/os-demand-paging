#ifndef _VMM_H_
#define _VMM_H_

#include "stdint.h"
#include "refs.h"
#include "fs.h"
#include "locks.h"

// range node, linked list
class FileRangeNode {
public:
    StrongPtr<File> file;
    uint32_t start;
    uint32_t end;
    StrongPtr<FileRangeNode> next;

    FileRangeNode(StrongPtr<File> f, uint32_t vaStart);
    virtual ~FileRangeNode();
    void printNode();
};

class FileList {
    StrongPtr<FileRangeNode> head;
    StrongPtr<FileRangeNode> last;
    StrongPtr<BlockingLock> lock;
public:
    FileList();
    virtual ~FileList();
    StrongPtr<FileRangeNode> getFileRangeNode(uint32_t va);
    void addFileRangeNode(StrongPtr<FileRangeNode> node);
    void printList();
};


// The physical memory interface
class PhysMem {
public:
    static constexpr uint32_t FRAME_SIZE = (1 << 12);
    static void init(uint32_t start, uint32_t end);

    /* allocate a frame */
    static uint32_t alloc();

    /* free a frame */
    static void free(uint32_t);
};

class AddressSpace {
    uint32_t *pd;
    uint32_t& getPTE(uint32_t va);
    StrongPtr<FileList> fileList;
    StrongPtr<BlockingLock> lock;
public:
    static void activate(StrongPtr<AddressSpace> as);
    static constexpr uint32_t P = 1;
    static constexpr uint32_t W = 2;
    static constexpr uint32_t U = 4;

    AddressSpace();
    virtual ~AddressSpace();
    void pmap(uint32_t va, uint32_t pa, bool forUser, bool forWrite);
    void map(uint32_t va, StrongPtr<File> file);
    void handlePageFault(uint32_t va);
};

#endif
