#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "vmm.h"
#include "ide.h"
#include "fat439.h"

volatile int togo;

void equals(const char x, const char y)
{
        if (x != y)
                Debug::panic("Fatal: Expected '%c', found '%c'.", y, x);
}
void checkHello(uint32_t addr)
{
        for (int i = 0; i < 5; i++) {
                char c = ((char*)addr)[i];
                switch (i) {
                case 0:
                        equals(c, 'h');
                        break;
                case 1:
                        equals(c, 'e');
                        break;
                case 2:
                case 3:
                        equals(c, 'l');
                        break;
                case 4:
                        equals(c, 'o');
                        break;
                }
        }
        getThenIncrement(&togo, -1);
}

void kernelMain(void)
{
        StrongPtr<BlockIO> d = IDE::d();
        StrongPtr<Fat439> fs { new Fat439(d) };
        StrongPtr<Directory> root = fs->rootdir;

        StrongPtr<AddressSpace> vm { new AddressSpace() };

        AddressSpace::activate(vm);

        StrongPtr<File> f = root->lookupFile("hello.txt");
        uint32_t a = 0x90000000;
        vm->map(a, f);
        constexpr int threads = 10;
        togo = threads;
        for (int i = 0; i < threads; i++)
                threadCreate<uint32_t>(checkHello, a);
        while (togo > 0)
                Thread::yield();
        Debug::say("all done");
}
void kernelTerminate() {}
