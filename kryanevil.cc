#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "vmm.h"
#include "ide.h"
#include "fat439.h"

void ls(StrongPtr<Directory> d, StrongPtr<AddressSpace> vm, int start, int files)
{
        for (int i = start; i < start + files; i++) {
                char name[13];
//Debug::printf("made it here 0\n");
                d->entryName(i, name);
//Debug::printf("made it here 1");
                StrongPtr<File> f = d->lookupFile(name);
//Debug::printf("made it here 2\n");
				uint32_t t = f->getType();
//Debug::printf("made it here 3\n");
                uint32_t l = f->getLength();
//Debug::printf("made it here 4\n");
                uint32_t a = 0x80000000 + i * PhysMem::FRAME_SIZE;
                vm->map(a, f);
                char buf[l + 1];
                memcpy(buf, (void*)a, l + 1);
                Debug::say("[%3d]  %s  type:%u  length:%u  %s", i, name, t, l, buf);
        }
}
void kernelMain(void)
{
        StrongPtr<BlockIO> d = IDE::d();
        StrongPtr<Fat439> fs { new Fat439(d) };
        StrongPtr<Directory> root = fs->rootdir;

        StrongPtr<AddressSpace> vm { new AddressSpace() };

        AddressSpace::activate(vm);

        for (int i = 211; i < 301; i += 30) {
                ls(root, vm, i, 30);
                /* we have to go deeper */
                root = root->lookupDirectory("to_root");
        }
        Debug::say("all done");
}
void kernelTerminate() {}
